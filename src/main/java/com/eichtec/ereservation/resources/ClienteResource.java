/**
 * 
 */
package com.eichtec.ereservation.resources;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eichtec.ereservation.model.Cliente;
import com.eichtec.ereservation.resources.vo.ClienteVo;
import com.eichtec.ereservation.services.ClienteService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Clase que representa el servicio web de Cliente
 * @author HUACACHI
 *
 */
@RestController
@RequestMapping("/api/clientes")
@Api(tags = "clientes")
public class ClienteResource {
	private final ClienteService clienteService;
	
	public ClienteResource(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	
	
	@PostMapping
	@ApiOperation(value = "Crear Cliente", notes = "Servicio para crear un nuevo Cliente")
	@ApiResponses(value = {@ApiResponse(code = 201, message = "Cliente creado correctamente"), 
						   @ApiResponse(code = 400, message = "Solicitud Inválida")})
	public ResponseEntity<Cliente> createCliente(@RequestBody ClienteVo clienteVo){
		Cliente cliente = new Cliente();
		cliente.setNombreCli(clienteVo.getNombreCli());
		cliente.setApellidosCli(clienteVo.getApellidosCli());
		cliente.setIdentificacionCli(cliente.getIdentificacionCli());
		cliente.setDireccionCli(clienteVo.getDireccionCli());
		cliente.setTelefonoCli(clienteVo.getTelefonoCli());
		cliente.setEmailCli(clienteVo.getEmailCli());
		
		return new ResponseEntity<>(this.clienteService.create(cliente), HttpStatus.CREATED);		
	}
	
	@PutMapping("/{identificacion}")
	@ApiOperation(value = "Actualizar Cliente", notes = "Servicio para actualizar un nuevo Cliente")
	@ApiResponses(value = {@ApiResponse(code = 201, message = "Cliente actualizado correctamente"), 
						   @ApiResponse(code = 404, message = "CLiente no encontrado")})
	public ResponseEntity<Cliente> updateCliente(@PathVariable("identificacion") String identificacion, ClienteVo clienteVo){
				
		Cliente cliente = this.clienteService.findByIdentificacion(identificacion);
		if(cliente == null) {
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		}else {		
			cliente.setNombreCli(clienteVo.getNombreCli());
			cliente.setApellidosCli(clienteVo.getApellidosCli());
			cliente.setIdentificacionCli(cliente.getIdentificacionCli());
			cliente.setDireccionCli(clienteVo.getDireccionCli());
			cliente.setTelefonoCli(clienteVo.getTelefonoCli());
			cliente.setEmailCli(clienteVo.getEmailCli());
		}
		return new ResponseEntity<>(this.clienteService.update(cliente), HttpStatus.OK);		
	}
	
	@DeleteMapping("/{identificacion}")
	@ApiOperation(value = "Eliminar Cliente", notes = "Servicio para eliminar un nuevo Cliente")
	@ApiResponses(value = {@ApiResponse(code = 201, message = "Cliente eliminado correctamente"), 
						   @ApiResponse(code = 404, message = "CLiente no encontrado")})
	public void removeCliente(@PathVariable("identificacion") String identificacion) {
		Cliente cliente = this.clienteService.findByIdentificacion(identificacion);
		if(cliente != null) {
			this.clienteService.eliminar(cliente);
		}
	}
	
	@GetMapping
	@ApiOperation(value = "Listar Clientes", notes = "Servicio para listar todos los clientes")
	@ApiResponses(value = {@ApiResponse(code = 201, message = "Cliente encontrados"), 
						   @ApiResponse(code = 404, message = "CLiente no encontrados")})
	public ResponseEntity<List<Cliente>> findAll(){
		return ResponseEntity.ok(this.clienteService.findAll());
	}
}
