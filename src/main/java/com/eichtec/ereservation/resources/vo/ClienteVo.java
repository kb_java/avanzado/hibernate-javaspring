/**
 * 
 */
package com.eichtec.ereservation.resources.vo;

import lombok.Data;

/**
 * Clase que representa a la tabla Cliente *
 * @author HUACACHI
 *
 */
@Data
public class ClienteVo {
	private String nombreCli;
	private String apellidosCli;
	private String identificacionCli;
	private String direccionCli;
	private String telefonoCli;
	private String emailCli;
	
	public ClienteVo() {
		// TODO Auto-generated constructor stub
	}	
}



