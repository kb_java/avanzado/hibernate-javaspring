/**
 * 
 */
package com.eichtec.ereservation.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.eichtec.ereservation.model.Reserva;

/**
 * Insterfaz para definit las operaciones de BD relacionadas con Reserva
 * @author HUACACHI
 *
 */
public interface ReservaRepository extends JpaRepository<Reserva, String> {
	/**
	 * Definiendo el método para buscar Reservas con fecha de Ingreso y de Salida.
	 * @param fechaIngreso
	 * @param fechaSalida
	 * @return
	 */
	@Query("SELECT r FROM Reserva r WHERE r.fechaIngresoRes =: fechaIngreso AND r.fechaSalidaRes =: fechaSalida")
	public List<Reserva> find(@Param("fechaIngreso") Date fechaIngreso, @Param("fechaSalida") Date fechaSalida);

	
}
