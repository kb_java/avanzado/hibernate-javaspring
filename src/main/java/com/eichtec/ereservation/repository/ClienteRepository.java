/**
 * 
 */
package com.eichtec.ereservation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eichtec.ereservation.model.Cliente;

/**
 * Interface para definir las operaciones de bd relacionadas con cliente
 * @author HUACACHI
 *
 */
public interface ClienteRepository extends JpaRepository<Cliente, String> {
	/**
	 * Definición de método para buscar a los clientes por su apellido 
	 * @param apellidosCli
	 * @return
	 */
	public List<Cliente> findByApellidosCli(String apellidosCli);
	
	/**
	 * Definición de método para buscar un Cliente por su identificación
	 * @param identificacionCli
	 * @return
	 */
	public Cliente findByIdentificacion(String identificacionCli); 
}
